﻿# HideFullDocInfoLink

## Component Information
* Author: Jonathan Hult
* Website: http://jonathanhult.com
* Last Updated: build_2_20131008
* License: MIT

## Overview
This component hides the Full/Default Document Information link on Content Information pages. The component can be turned on/off via the preference prompt HideFullDocInfoLink_ComponentEnabled.
	
* Dynamichtml includes:
	- docinfo_page_title: Core - 10g - override to hide Full Info link on Content Information page
	- docinfo_menus_setup: Core - override to include docinfo_menus_setup_hide_full_default
	- docinfo_menus_setup_hide_full_default: HideFullDocInfoLink - 11g - Remove Full Information and Default Information links on Content Information page

* Preference Prompts:
	- HideFullDocInfoLink_ComponentEnabled: Boolean determining whether component functionality is enabled

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.0-2013-07-11 17:07:21Z-r106802 (Build: 7.3.5.185)

## Changelog
* build_2_20131008
	- Added 11g compatibility
* build_1_20120705
	- Initial component release